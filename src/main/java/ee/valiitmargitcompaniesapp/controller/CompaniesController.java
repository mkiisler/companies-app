package ee.valiitmargitcompaniesapp.controller;

import ee.valiitmargitcompaniesapp.model.Company;
import ee.valiitmargitcompaniesapp.repository.CompaniesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin("*")

public class CompaniesController {

    @Autowired
    private CompaniesRepository companiesRepository;


    @GetMapping("/companies")
    public List<Company> getCompanies() {
        return companiesRepository.fetchCompanies();

    }

    @GetMapping("/company")
    public Company getCompany(@RequestParam("id") int id) {
        return companiesRepository.fetchCompany(id);
    }

    @DeleteMapping("/company")
    public void deleteCompany(@RequestParam("id") int id) {
        companiesRepository.deleteCompany(id);
    }

    @PostMapping("/company")
    public void addCompany(@RequestBody Company x) {
        companiesRepository.addCompany(x);

    }

    @PutMapping("/company")
    public void updateCompany(@RequestBody Company y) {
        companiesRepository.updateCompany(y);
    }


    /*@GetMapping
    public String getHello(@RequestParam("name") String nameParam, @RequestParam("profession") String
            professionParam) {
        return "Tere, " + nameParam + ". Sa oled ametilt " + professionParam;

    }


    // Klassi ja selle meetodi defineerimine teise klassi sees:

    @PostMapping("/person")
    public void savePerson(@RequestBody Person x) {
        System.out.println("Me saime sellise sisendi: " + x.getName() + ", " + x.getProfession());

    }

    @GetMapping("/employees")
    public List<Person> getEmployees() {
        return Arrays.asList(
                new Person("Marek", "arendaja"),
                new Person("Teet", "müügijuht"),
                new Person("Malle", "tegevjuht"),
                new Person("Rita", "pressiesindaja")

        );
    }


    public static class Person {
        private String name;
        private String profession;
        // defineerime objekti


        public Person(String name) {
            this.name = name;
        }

        public Person(String name, String profession) {
            this.name = name;
            this.profession = profession;
        }


        public void setName(String name) {
            this.name = name;
        }

        public void setProfession(String profession) {
            this.profession = profession;
        }

        public String getName() {
            return name;
        }

        public String getProfession() {
            return profession;
        }
    }*/

}
