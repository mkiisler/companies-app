package ee.valiitmargitcompaniesapp.repository;

import ee.valiitmargitcompaniesapp.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompaniesRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Company> fetchCompanies() {
        List<Company> companies = jdbcTemplate.query
                ("SELECT * FROM company",
                        (row, rowNum) -> {
                            return new Company(
                                    row.getInt("id"),
                                    row.getString("name"),
                                    row.getString("logo")

                            );
                        }
                );
        return companies;
    }


    public Company fetchCompany(int id) {
        List<Company> companies = jdbcTemplate.query
                ("SELECT * FROM company WHERE ID = ?",
                        new Object[]{id},
                        (row, rowNum) -> {
                            return new Company(
                                    row.getInt("id"),
                                    row.getString("name"),
                                    row.getString("logo")

                            );
                        }
                );
        return companies.size() > 0 ? companies.get(0) : null;


    }

    public void deleteCompany(int id) {
        jdbcTemplate.update("DELETE FROM company WHERE id = ?", id);

    }

    public void addCompany(Company company) {
        jdbcTemplate.update("INSERT INTO company (name, logo) VALUES (?, ?)", company.getName(), company.getLogo());
    }

    public void updateCompany(Company company){
        jdbcTemplate.update("UPDATE company SET name = ?, logo = ? WHERE id = ?",
                company.getName(), company.getLogo(), company.getId());

    }

}
