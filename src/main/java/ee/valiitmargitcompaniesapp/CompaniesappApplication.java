package ee.valiitmargitcompaniesapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompaniesappApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompaniesappApplication.class, args);
    }

}
